<?php

namespace justjob\models;


use Illuminate\Database\Eloquent\Model;

class Categorie extends Model {

    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;



    public static function toutesLesCategories(){
        return self::select('*')->get();
    }

    public static function chercherUneCategorie($p_id){
        return self::where('id', '=', $p_id)->first();
    }

}