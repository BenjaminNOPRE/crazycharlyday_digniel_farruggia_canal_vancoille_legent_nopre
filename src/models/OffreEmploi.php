<?php

namespace justjob\models;


use Illuminate\Database\Eloquent\Model;

class OffreEmploi extends Model {

    protected $table = 'offreemploi';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public static function tousLesOffresdEmploi(){
        return self::select('*')->get();
    }

    public static function chercherUneOffre($p_id){
        return self::where('id', '=', $p_id)->first();
    }


}