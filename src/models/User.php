<?php

namespace justjob\models;


use Illuminate\Database\Eloquent\Model;

class User extends Model {

    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public static function tousLesUsers(){
        return self::select('*')->get();
    }

    public static function chercherUnUser($p_id){
        return self::where('id', '=', $p_id)->first();
    }

}