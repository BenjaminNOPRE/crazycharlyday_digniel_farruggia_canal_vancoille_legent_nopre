<?php

namespace justjob\models;


use Illuminate\Database\Eloquent\Model;

class Transport extends Model {

    protected $table = 'transports';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public static function tousLesTransports(){
        return self::select('*')->get();
    }

    public static function chercherUnTransports($p_id){
        return self::where('id', '=', $p_id)->first();
    }

    public static function chercherUnTypedeVehicule($p_id){
        return self::where('id', '=', $p_id)->first();
    }
    public static function chercherUnEtatdeCandidature($p_id){
        return self::where('id', '=', $p_id)->first();
    }


}