<?php

namespace justjob\models;


use Illuminate\Database\Eloquent\Model;

class Candidature extends Model {

    protected $table = 'candidatureemploi';
    protected $primaryKey = 'id_candidature';
    public $timestamps = false;



    public static function toutesLesCandidatures(){
        return self::select('*')->get();
    }

    public static function chercherUneCandidatureParId($p_id){
        return self::where('id_candidature', '=', $p_id)->first();
    }

    public static function chercherUneCandidatureParIdCandidat($p_id){
        return self::where('id_candidat', '=', $p_id)->get();
    }

}