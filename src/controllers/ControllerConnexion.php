<?php

namespace justjob\controllers;

use justjob\views\ViewConnexion;


class ControllerConnexion {

    public static function afficherComptes(){
        ViewConnexion::afficherComptes();
    }

    public static function afficheConnecte() {
        ViewConnexion::afficherConnecte();
    }

}