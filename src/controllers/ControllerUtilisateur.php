<?php

namespace justjob\controllers;


use justjob\views\ViewUtilisateur;

class ControllerUtilisateur {

    public static function afficheUtilisateurs(){
        ViewUtilisateur::afficherUtilisateurs();
    }

    public static function afficheUnUtilisateur($id){
        ViewUtilisateur::afficherUnUtilisateur($id);
    }

}