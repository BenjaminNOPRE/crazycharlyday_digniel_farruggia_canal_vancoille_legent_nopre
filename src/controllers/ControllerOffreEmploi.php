<?php

namespace justjob\controllers;


//use justjob\models\OffreEmploi;
//use justjob\views\ViewCreationOffre;


class ControllerOffreEmploi {

    //l affichage
    public function afficher($choix) {
        $offreE = OffreEmploi::get();
        $offreE->tousLesOffresdEmploi();
    }

    public function creerOffre($nomutilisateur, $lieu, $nomcategorie, $description, $duree, $id_utilisateur) {
        $user = User::select('id')->where('nom', '=', $nomutilisateur)->first();
        $categ = Categorie::select('id')->where('nom', '=', $nomcategorie)->first();
        $offreEmploi = new OffreEmploi();
        $offreEmploi->lieu = $lieu;
        $offreEmploi->id_categorie = $categ;
        $offreEmploi->description = $description;
        $offreEmploi->duree = $duree;
        $offreEmploi->id_utilisateur = $user;
        $offreEmploi->save();

    }

}