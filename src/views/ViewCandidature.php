<?php

namespace justjob\views;


use justjob\models\Candidature;

class ViewCandidature {

    public static function afficherCandidaturesParCandidat($id){

        $html = "<div> Voici vos candidatures ($id) : ";

        $html .= "<ul>";

        foreach (Candidature::chercherUneCandidatureParIdCandidat($id) as $candidature) {
            $html .= "<li> $candidature </li>";
        }

        $html .= "</ul>";

        echo $html;

    }

}