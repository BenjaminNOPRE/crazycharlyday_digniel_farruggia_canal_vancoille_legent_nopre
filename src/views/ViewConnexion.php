<?php

namespace justjob\views;

use justjob\models\User;

class ViewConnexion {

    public static function afficherComptes(){
        $comptes = User::tousLesUsers();

        $html = "<html>";
        $html .= "<head>";
        $html .= "<link rel=stylesheet type=text/css href='../css/style.css'>";
        $html .= "<title>Connexion</title>";
        $html .= "</head>";
        $html .= "<body>";
        $html .= "<header>";
        $html .= "<h1><a></a></h1>";
        $html .= "</header>";
        $html .= "<div> Choisissez votre compte :</div>";
        $html .= "<a href='..'>Retour</a>";
        $html .= "<ul>";

        foreach ($comptes as $compte) {
            $html .= "<li> <a href='connecte/$compte->id'>$compte->nom</a></li>";
        }

        $html .= "</ul>";
        $html .= "</div>";
        $html .= "</body>";
        $html .= "</html>";
        echo $html;
    }

    public static function afficherConnecte(){

        $sess = $_SESSION['user'];

        $html = <<<END
        <html>
        <head>
        <link rel=stylesheet type=text/css href=../css/style.css>
        <title>Accueil</title>
        </head>
        <body>
        <header>
        <h1><a></a></h1>
        </header>
        <div> Bienvenue sur notre site !</div>
        <ul>

        <li> <a href="../index.php">Se deconnecter</a> </li>

        <li> <a href="utilisateurs">Liste des utilisateurs</a> </li>
        
        
        
        <li> <a href="candidatures/$sess">Liste de vos candidatures</a> </li>

        </ul>
        </body>
        </html>
END;
    echo $html;

    }

}
