<?php

namespace justjob\views;


use justjob\models\User;

class ViewUtilisateur {

    public static function afficherUtilisateurs(){

      $html = "<html>";
      $html .= "<head>";
      $html .= "<link rel=stylesheet type=text/css href=css/style.css>";
      $html .= "<title>Users</title>";
      $html .= "</head>";
      $html .= "<body>";
      $html .= "<header>";
      $html .= "<h1><a></a></h1>";
      $html .= "</header>";
      $html .= "<ul>";
        $html = "<a href='connecte'>Retour</a>";

        $html .= "<ul>";

        foreach (User::tousLesUsers() as $compte) {

            $html .= "<li> <a href='utilisateurs/$compte->id'>$compte->nom</a> </li>";

        }
        $html .= "</ul>";
        $html .= "</div>";
        $html .= "</body>";
        $html .= "</html>";
        echo $html;

    }

    public static function afficherUnUtilisateur($id){

        $html = "<a href='../utilisateurs'>Retour</a><br><br>";

        $html .= User::chercherUnUser($id);

        echo $html;
    }

}
