<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once 'vendor/autoload.php';

use Slim\Slim as Slim;

use Illuminate\Database\Capsule\Manager as Manager;
use \justjob\controllers\ControllerAccueil;
use justjob\controllers\ControllerConnexion;
use \justjob\controllers\ControllerUtilisateur;
use  \justjob\controllers\ControllerCandidature;


$db = new Manager();


session_start();



$db->addConnection( [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'crazycharly',
    'username' => 'user',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
] );

$db->setAsGlobal();
$db->bootEloquent();


$app = new Slim();


$app->get('/', function(){
    if (isset($_SESSION['user'])) ControllerAccueil::afficherAccueil();
});

$app->get('/connexion', function (){
    ControllerConnexion::afficherComptes();
});

$app->get('/connecte', function (){
    ControllerConnexion::afficheConnecte();
    echo $_SESSION['user'];
});

$app->get('/connecte/:id', function ($id){
    $_SESSION['user'] = $id;
    echo "<script type='text/javascript'>document.location.replace('../connecte');</script>";
});

$app->get('/utilisateurs', function (){
    ControllerUtilisateur::afficheUtilisateurs();
});

$app->get('/utilisateurs/:id', function ($id){
    ControllerUtilisateur::afficheUnUtilisateur($id);
});

$app->get('/candidatures/:id', function ($id){
    ControllerCandidature::afficherCandidaturesParCandidat($id);
});


$app->run();
